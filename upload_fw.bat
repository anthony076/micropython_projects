:: ==== 將 NodeMCU 的 FW 換成 Micropython ====
:: 清除 flash
esptool.py --port COM3 erase_flash
:: 燒入 flash
:: esptool.py --chip esp8266 --port COM3 --baud 460800 write_flash --flash_size detect 0x0 "files\[fw] esp8266-20210202-v1.14.bin"
:: 若燒錄後無法使用，改用以下命令
esptool.py --chip esp8266 --port COM3 --baud 460800 write_flash --flash_mode dio --flash_size detect 0x0 "files\[fw] esp8266-20210202-v1.14.bin"
pause